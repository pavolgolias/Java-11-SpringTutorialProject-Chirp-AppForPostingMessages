package sk.pgl.spring.chirp.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import sk.pgl.spring.chirp.entity.User;
import sk.pgl.spring.chirp.repository.UserRepository;

@Service
public class UserSecurityService implements UserDetailsService {
    private static final Logger log = LoggerFactory.getLogger(UserSecurityService.class);

    private UserRepository userRepository;

    @Autowired
    public UserSecurityService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);

        if (user == null) {
            log.warn("Username not found!");
            throw new UsernameNotFoundException("Username " + username + " was not found!");
        }

        return user;
    }
}
