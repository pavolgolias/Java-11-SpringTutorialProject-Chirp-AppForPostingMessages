package sk.pgl.spring.chirp.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import sk.pgl.spring.chirp.entity.security.UserRole;

public interface UserRoleRepository extends MongoRepository<UserRole, String> {
}
