package sk.pgl.spring.chirp.service;

import org.json.JSONException;
import org.springframework.http.HttpStatus;
import sk.pgl.spring.chirp.entity.User;
import sk.pgl.spring.chirp.entity.security.UserRole;

import java.io.IOException;
import java.util.Set;

public interface UserService {
    User createUser(User user, Set<UserRole> userRoles);

    HttpStatus validateToken(String token) throws IOException, JSONException;

    void setUserSession(String username);
}
