package sk.pgl.spring.chirp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChirpBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChirpBackendApplication.class, args);
	}
}
