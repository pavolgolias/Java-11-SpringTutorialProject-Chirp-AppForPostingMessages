package sk.pgl.spring.chirp.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import sk.pgl.spring.chirp.entity.security.Role;

public interface RoleRepository extends MongoRepository<Role, String> {

}
