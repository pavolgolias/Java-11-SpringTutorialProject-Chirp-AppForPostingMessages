﻿## Spring Tutorial Project - Chirp - App for posting messages
> Small Spring tutorial project. Application provides interface for signup and login for users and posting new messages after successful login. Project has two parts - frontend part implemented in Vue.js and backend implemented with Spring and MongoDB.
 
 **Used technologies:**
 - [Spring](https://spring.io/) - SPring Boot, Spring Security, Spring Data, REST, Maven
 - [Vue.js](https://vuejs.org/), [NPM](https://www.npmjs.com/), [Node.js](https://nodejs.org/en/), [webpack](https://webpack.github.io/), [vue-cli](https://github.com/vuejs/vue-cli), [vuestart boilerplate](https://github.com/ssouron/vuestart)
 - [MongoDB](https://www.mongodb.com/), [3T MongoChef - The GUI for MongoDB](http://3t.io/mongochef/), [Mongo Morphia](https://github.com/mongodb/morphia)

